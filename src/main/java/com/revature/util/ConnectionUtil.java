package com.revature.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String connectionString = "jdbc:postgresql://training-db.cjk8aacizjxv.us-east-1.rds.amazonaws" +
                ".com:5432/postgres";

        // java app will fetch these values from the environment
        String user = System.getenv("DB_USER");
        String pass = System.getenv("DB_PASS");

        return DriverManager.getConnection(connectionString, user, pass);

    }
}
