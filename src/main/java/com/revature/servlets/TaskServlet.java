package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.daos.TaskDAO;
import com.revature.daos.TaskDAOImpl;
import com.revature.models.Task;
import com.revature.models.TaskPriority;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;

public class TaskServlet extends HttpServlet {

    private TaskDAO taskDao = new TaskDAOImpl();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // return a list of tasks
        // invoke DAO method to obtain a list of tasks
        List<Task> taskList = taskDao.getAllTasks();

        // convert list into JSON string
        String taskJSON = objectMapper.writeValueAsString(taskList);

        resp.setHeader("Content-Type", "application/json");

        // write the JSON string to the response body
        try(PrintWriter pw = resp.getWriter()){
            pw.write(taskJSON);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // create a new task record

        // get the new task data from our request - getParameter allows us to access request params
        String description = req.getParameter("description"); // this would match the name in our input

        // all of our request parameters are sent in the format of a string
            // so if we need these parameters to be of a different type,
            // we need to perform these conversions ourselves
        String priorityString = req.getParameter("priority"); // this would match the name in our input
        TaskPriority priority = TaskPriority.valueOf(priorityString);
        String dueDateString = req.getParameter("due-date"); // this would match the name in our input
        LocalDate dueDate = LocalDate.parse(dueDateString);

        // create a new task object, and populate it with the params
        Task task = new Task();
        task.setDescription(description);
        task.setPriority(priority);
        task.setDueDate(dueDate);

        // use the DAO method to add the task to the db
        boolean success = taskDao.create(task);
        if(success){
            resp.setStatus(201);
        } else {
            resp.setStatus(500);
        }
    }
}
