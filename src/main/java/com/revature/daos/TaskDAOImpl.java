package com.revature.daos;

import com.revature.models.Task;
import com.revature.models.TaskPriority;
import com.revature.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TaskDAOImpl implements TaskDAO {


    @Override
    public boolean create(Task newTask) {
        // validate newTask input to make sure it's not null
        if(newTask==null){
            return false;
        }
        // validate newTask input to make sure it has all of the important fields
        if(newTask.getPriority()==null || newTask.getDescription()==null || newTask.getDueDate()==null){
            return false;
        }

        // establish connection
        try(Connection connection = ConnectionUtil.getConnection();){
            // create a prepared statement
            PreparedStatement ps = connection.prepareStatement("insert into task values (default, ?, ?, ?, ?)");

            // parameterize the '?' in the prepared statement with task values
            ps.setBoolean(1, newTask.isComplete());
            ps.setString(2, newTask.getDescription());
            // Task: taskId=0, isComplete=false, description=null, priority=null, date=null
            ps.setInt(3, newTask.getPriority().ordinal());
            ps.setObject(4, newTask.getDueDate());

            // execute prepared statement -> number of rows affected
            int numOfRows = ps.executeUpdate();

            // return true/false based on the number of rows affected
            if(numOfRows==1){
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Task> getAllTasks() {
        try(Connection connection = ConnectionUtil.getConnection();
            Statement s = connection.createStatement()){
            ResultSet rs = s.executeQuery("select * from task");

            List<Task> tasks = new ArrayList<>();



            while(rs.next()){
                int id = rs.getInt("task_id");
                boolean isComplete = rs.getBoolean("is_complete");
                String description = rs.getString("description");

                // access priority ordinal from the db
                int priorityOrdinal = rs.getInt("priority");
                // then use that number to determine the corresponding enum value
                TaskPriority[] priorities = TaskPriority.values();
                TaskPriority priority = priorities[priorityOrdinal];
                /*
                // if we had stored our priority as a string, we would access it like this:
                String priorityString = rs.getString("priority");
                TaskPriority priority = TaskPriority.valueOf(priorityString);
                */
                LocalDate dueDate = rs.getObject("due_date", LocalDate.class);
                Task t = new Task(id, isComplete, description, priority, dueDate);
                tasks.add(t);
            }
            return tasks;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Task> getTasksByDescription(String description) {
        // create connection
        // create prepared statement, parameterized with description
        // execute query and process the result set
        // return the appropriate list
        return null;

    }

    @Override
    public Task getTaskById(int id) {
        // we can return a task object for this bc id is a unique field
            // (there will only ever be one with a particular id)
        return null;
    }
}
