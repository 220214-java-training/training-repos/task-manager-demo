package com.revature.daos;

import com.revature.models.Task;

import java.util.List;

public interface TaskDAO {

    /*
        define all of the methods we expect our application to need in its db communication
     */

    // adding a new task object to the db
        // boolean represents success in creating the record
    public boolean create(Task task);

    // getting all task objects from db
        // List<Task> represents result set from db
    public List<Task> getAllTasks();

    public List<Task> getTasksByDescription(String description);

    public Task getTaskById(int id);

}
