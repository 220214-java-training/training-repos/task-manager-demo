package com.revature.util;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ConnectionUtilTest {

    @Test
    public void testConnection() throws SQLException {
        Connection connection = ConnectionUtil.getConnection();
        String driverName = connection.getMetaData().getDriverName();
        assertNotNull(driverName);
    }

}
