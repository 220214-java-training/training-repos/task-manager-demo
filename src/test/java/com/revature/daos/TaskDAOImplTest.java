package com.revature.daos;

import com.revature.models.Task;
import com.revature.models.TaskPriority;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TaskDAOImplTest {

    private TaskDAO taskDAO = new TaskDAOImpl();

    @Test
    public void validTaskCreationReturnsTrue(){
        Task t = new Task(); // taskId = 0, isCompleted = false, description = null, dueDate = null
        t.setDescription("wash car");
        t.setDueDate(LocalDate.of(2022, 3,24));
        t.setPriority(TaskPriority.MEDIUM);
        boolean actual = taskDAO.create(t);
        assertTrue(actual);
    }

    @Test
    public void taskWithDefaultValuesReturnsFalse(){
        Task t = new Task();
        boolean actual = taskDAO.create(t);
        assertFalse(actual);
    }

    @Test
    public void nullTaskValuesReturnsFalse(){
        boolean actual = taskDAO.create(null);
        assertFalse(actual);
    }

    @Test
    public void getAllReturnsList(){
        List<Task> tasks = taskDAO.getAllTasks();
        // it is good practice to always use assertAll when using multiple assertions
        assertAll(()->{
            assertNotNull(tasks);
            assertTrue(tasks.size()>0);
        });
    }

}
