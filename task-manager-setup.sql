create table task(
	task_id serial primary key,
	is_complete boolean,
	description varchar(100) not null,
	priority integer check (priority<4),
	due_date Date check (due_date>current_date)
)

insert into task values (default, false, 'take out trash', 2, '2022-03-18');
insert into task values (default, true, 'empty dishwasher', 0, '2022-03-18');
insert into task values (default, false, 'do laundry', 1, '2022-03-20');
